window.onload = function() {
  // Market listing modal:
  var marketListModal = document.getElementById("marketListModal");
  // Get the button that opens the modal
  var marketListBtn = document.getElementById("marketListBtn");
  // Get the <span> element that closes the modal
  var marketListSpan = document.getElementsByClassName("custom-close")[0];
  // When the user clicks on the button, open the modal
  marketListBtn.onclick = function() { marketListModal.style.display = "block"; }
  // When the user clicks on <span> (x), close the modal
  marketListSpan.onclick = function() { marketListModal.style.display = "none"; }
  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == marketListModal) {
      modal.style.display = "none";
    }
  }
}
